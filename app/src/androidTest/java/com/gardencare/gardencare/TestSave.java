package com.gardencare.gardencare;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;


import com.gardencare.gardencare.Builder.PlantBuilder;
import com.gardencare.gardencare.Builder.SpecieBuilder;

import com.gardencare.gardencare.Model.Plant;
import com.gardencare.gardencare.Model.Specie;

import org.junit.Test;
import org.junit.runner.RunWith;


import java.util.Date;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class TestSave {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        Specie s = (Specie) new SpecieBuilder().build(2);

        Plant plant = new Plant();
        plant.setMature("no");
        plant.setPlantedAt(new Date());
        plant.setSpecie(s);
        plant.save(1);

        Plant p2 = (Plant) new PlantBuilder().build(plant.getIdPlant());
        assertEquals(plant.getIdPlant(),p2.getIdPlant());
    }
}