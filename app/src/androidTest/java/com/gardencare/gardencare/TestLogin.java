package com.gardencare.gardencare;


import android.support.test.runner.AndroidJUnit4;

import com.gardencare.gardencare.WebServices.LoginService;

import org.junit.Test;
import org.junit.runner.RunWith;


import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class TestLogin {
    @Test
    public void useAppContext() throws Exception {
        assertEquals(LoginService.checkLogin("",""),"true");
    }
}