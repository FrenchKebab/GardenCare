package com.gardencare.gardencare;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.gardencare.gardencare.Builder.PersonBuilder;

import com.gardencare.gardencare.Model.Person;

import org.junit.Test;
import org.junit.runner.RunWith;


import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class TestPersonBuilder {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        Person p = (Person) new PersonBuilder().build(1);

        assertEquals(p.getGardens().get(0).getPlants().size(),2);
    }
}