package com.gardencare.gardencare.Views;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.gardencare.gardencare.Builder.PersonBuilder;
import com.gardencare.gardencare.Model.Garden;
import com.gardencare.gardencare.Model.Person;
import com.gardencare.gardencare.Model.Sky;
import com.gardencare.gardencare.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button gardens = (Button) findViewById(R.id.gardens);

        gardens.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MyGardens.class));
            }
        });

        Button disconnect = (Button) findViewById(R.id.disconnect);
        disconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences prefs = getApplicationContext().getSharedPreferences(
                        "com.example.app", Context.MODE_PRIVATE);
                SharedPreferences.Editor e = prefs.edit();
                e.remove("email");
                e.apply();

                startActivity(new Intent(MainActivity.this, Login.class));
            }
        });

        new WeatherService().execute();

    }

    private class WeatherService extends AsyncTask<Void,Void,String>{

        @Override
        protected String doInBackground(Void... voids) {
            try {

                String login = Login.getLogin(getApplicationContext().getSharedPreferences("com.example.app", Context.MODE_PRIVATE));
                ArrayList<Garden> gardens = ((Person) new PersonBuilder().build(login)).getGardens();

                String infos = "";
                Sky sky = new Sky();
                Calendar.getInstance();
                String hour = String.valueOf(new Date().getHours());
                for(Garden garden : gardens){
                    infos += garden.getCity() + " : " + sky.getInfo(garden.getCity(),hour,"temp_c") + "°C" + System.getProperty("line.separator");
                }
                return infos;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            TextView weather = (TextView) findViewById(R.id.weatherInfo);
            weather.setText(s);
        }
    }

}
