package com.gardencare.gardencare.Views;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.gardencare.gardencare.R;
import com.gardencare.gardencare.WebServices.LoginService;


public class Login extends AppCompatActivity {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isConnected(getApplicationContext().getSharedPreferences("com.example.app", Context.MODE_PRIVATE))) {
            startActivity(new Intent(Login.this, MainActivity.class));
        }
        setContentView(R.layout.login);

        Button connect = (Button) findViewById(R.id.connect);

        connect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText editemail = (EditText) findViewById(R.id.editmail);
                EditText editpassword = (EditText) findViewById(R.id.password);

                String email = editemail.getText().toString();
                String password = editpassword.getText().toString();

                new LoginNetwork().execute(email,password);



            }
        });

    }

    private class LoginNetwork extends AsyncTask<String, Void, Boolean>{

        @Override
        protected Boolean doInBackground(String... strings) {

            try {
                return LoginService.checkLogin(strings[0],strings[1]);
            } catch (Exception e) {
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if (aBoolean) {
                SharedPreferences prefs = Login.this.getSharedPreferences(

                        "com.example.app", Context.MODE_PRIVATE);
                prefs.edit().putString("email", ((EditText) findViewById(R.id.editmail)).getText().toString()).apply();

                startActivity(new Intent(Login.this, MainActivity.class));
            }
            else{
                Toast toast = Toast.makeText(getApplicationContext(),"Invalid email/password",Toast.LENGTH_SHORT);
                toast.show();
            }

        }
    }

    public static boolean isConnected(SharedPreferences sharedPreferences) {
        sharedPreferences.getString("email", null);
        return sharedPreferences.getString("email", null) != null;
    }

    public static String getLogin(SharedPreferences sharedPreferences){
        return sharedPreferences.getString("email",null);
    }


}
