package com.gardencare.gardencare.Views;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.gardencare.gardencare.Builder.PersonBuilder;
import com.gardencare.gardencare.Model.Garden;
import com.gardencare.gardencare.Model.Person;
import com.gardencare.gardencare.R;

public class MyGardens extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gardens);

        FloatingActionButton addGarden = (FloatingActionButton) findViewById(R.id.addGarden);

        addGarden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MyGardens.this,AddGarden.class));
            }
        });
        new PersonGardens().execute(Login.getLogin(getApplicationContext().getSharedPreferences("com.example.app", Context.MODE_PRIVATE)));

    }

    private class PersonGardens extends AsyncTask<String, Void, Person>{
        @Override
        protected Person doInBackground(String... strings) {
            PersonBuilder pb = new PersonBuilder();
            try {
                return (Person) pb.build(strings[0]);
            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Person person) {
            for(final Garden garden : person.getGardens()){
                ContextThemeWrapper newContext = new ContextThemeWrapper(MyGardens.this, R.style.Button);
                Button b = new Button(newContext);
                b.setText(garden.toString());
                b.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MyGardens.this,MyPlants.class);
                        intent.putExtra("idGarden",garden.getIdGarden());

                        startActivity(intent);
                    }
                });
                LinearLayout layout = (LinearLayout) findViewById(R.id.linear_layout_gardens);
                layout.addView(b);
            }
        }
    }
}
