package com.gardencare.gardencare.Views;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.gardencare.gardencare.Builder.PlantBuilder;
import com.gardencare.gardencare.Model.Plant;
import com.gardencare.gardencare.R;

public class PlantActivity extends AppCompatActivity {
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plant);
        new PlantService().execute(getIntent().getIntExtra("idPlant",-1));


    }

    private class PlantService extends AsyncTask<Integer, Void, Plant>{

        @Override
        protected Plant doInBackground(Integer... ids) {
            try {
                return (Plant) new PlantBuilder().build(ids[0]);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(Plant plant) {
            CollapsingToolbarLayout toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.plantToolbarCollapsing);
            TextView plantToString = (TextView) findViewById(R.id.plantToString);

            toolbarLayout.setTitle(plant.getSpecie().getName());
            plantToString.setText(plant.toString());
        }
    }


}
