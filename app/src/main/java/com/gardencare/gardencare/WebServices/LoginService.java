package com.gardencare.gardencare.WebServices;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;


public final class LoginService {
    public static boolean checkLogin(String mail, String password) throws Exception {
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("http://149.202.175.71/GardenCareService/public/login");
        httpPost.addHeader("Content-type", "application/x-www-form-urlencoded");
        BasicNameValuePair Vusername = new BasicNameValuePair("email", mail);
        BasicNameValuePair Vpassword = new BasicNameValuePair("password", password);

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(Vusername);
        params.add(Vpassword);
        UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(params);
        httpPost.setEntity(urlEncodedFormEntity);

        HttpResponse httpResponse = httpClient.execute(httpPost);
        InputStream inputStream = httpResponse.getEntity().getContent();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        StringBuilder stringBuilder = new StringBuilder();
        String bufferedStrChunk = null;

        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
            stringBuilder.append(bufferedStrChunk);
        }

        return Objects.equals(stringBuilder.toString(), "true");
    }
}
