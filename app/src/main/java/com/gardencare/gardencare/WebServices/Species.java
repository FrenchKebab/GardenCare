package com.gardencare.gardencare.WebServices;

import com.gardencare.gardencare.Model.Specie;
import com.loopj.android.http.HttpGet;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public final class Species {
    public static ArrayList<Specie> getSpecies() throws Exception {
        ArrayList<Specie> species = new ArrayList<>();
        HttpClient client = new DefaultHttpClient();
        String query = "http://149.202.175.71/GardenCareService/public/species";
        URI uri = new URI(query);
        HttpGet request = new HttpGet();
        request.setURI(uri);
        HttpResponse response = client.execute(request);
        String result = getResponse(response);
        JSONArray jsonArray = new JSONArray(result);
        ArrayList<String> list = new ArrayList<String>();
        for(int i = 0; i < jsonArray.length(); i++)
        {
            list.add(jsonArray.get(i).toString());
        }
        for(int j = 0 ; j < list.size() ; j++)
        {
            JSONObject o = new JSONObject(list.get(j));
            Specie specie = new Specie(o.get("name").toString());
            specie.setIdSpecie(Integer.parseInt(o.get("idSpecie").toString()));
            specie.setGrowTime(Integer.parseInt(o.get("growth_time").toString()));
            specie.setWetAdvice( o.get("wet_advice").toString());
            specie.setWetHour(o.get("wet_hour").toString());
            specie.setImage(o.get("image").toString());
            species.add(specie);
        }
        return  species;
    }

    public static Specie getSpecie(String name) throws Exception {
        HttpClient client = new DefaultHttpClient();
        String query = "http://149.202.175.71/GardenCareService/public/specie?name=" + name;
        URI uri = new URI(query);
        HttpGet request = new HttpGet();
        request.setURI(uri);
        HttpResponse response = client.execute(request);
        String result = getResponse(response);
        JSONObject o = new JSONObject(result);
        Specie specie = new Specie(o.get("name").toString());
        specie.setIdSpecie(Integer.parseInt(o.get("idSpecie").toString()));
        specie.setGrowTime(Integer.parseInt(o.get("growth_time").toString()));
        specie.setWetAdvice( o.get("wet_advice").toString());
        specie.setWetHour(o.get("wet_hour").toString());
        specie.setImage(o.get("image").toString());
        return  specie;
    }

    private static String getResponse(HttpResponse response) throws IOException {
        String result;
            InputStream in = response.getEntity().getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder str = new StringBuilder();
            String line = null;
            while((line = reader.readLine()) != null)
            {
                str.append(line).append("\n");
            }
            in.close();
            result = str.toString();

        return result;
    }
}

