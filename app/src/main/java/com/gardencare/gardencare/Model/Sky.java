package com.gardencare.gardencare.Model;

import android.util.Log;
import com.loopj.android.http.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

import org.json.*;


public class Sky {
    private final String key = "fde0a5a61cca4ad6a7a134523170404";

    public String getWeather(String city,String hour) throws Exception {
        HashMap hash = new HashMap();
            HttpClient client = new DefaultHttpClient();
            String query = "https://api.apixu.com/v1/forecast.json?key=" + key + "&q=" + city + "&hour=" + hour;
            URI uri = new URI(query);
            HttpGet request = new HttpGet();
            request.setURI(uri);
            HttpResponse response = client.execute(request);
        return getResponse(response);
    }

    /*
     * Method creating JSONObjects in order to get the temperature
     */
    public String getInfo(String city, String hour,String info) throws Exception {
        JSONObject weather = new JSONObject(this.getWeather(city,hour));
        JSONObject forecast = new JSONObject(weather.getString("forecast"));
        JSONObject forecastDay = (JSONObject) forecast.getJSONArray("forecastday").get(0);
        JSONObject forecastHour = (JSONObject) forecastDay.getJSONArray("hour").get(0);

        return forecastHour.getString(info);
    }


    private String getResponse(HttpResponse response) throws IOException {
        String result = "";

            InputStream in = response.getEntity().getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder str = new StringBuilder();
            String line = null;
            while((line = reader.readLine()) != null)
            {
                str.append(line).append("\n");
            }
            in.close();
            result = str.toString();

        return result;
    }
}
