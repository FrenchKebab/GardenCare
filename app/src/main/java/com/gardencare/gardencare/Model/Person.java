package com.gardencare.gardencare.Model;

import java.util.ArrayList;

public class Person{
    private int idPerson;
    private String firstname;
    private String name;
    private String email;
    private String password;
    private ArrayList<Garden> gardens;

    public Person()
    {
        this.gardens = new ArrayList<>();
    }

    public ArrayList<Garden> getGardens() {
        return gardens;
    }

    public void setGardens(ArrayList<Garden> gardens) {
        this.gardens = gardens;
    }

    public ArrayList<Plant> getPlants(){

        ArrayList<Plant> plants = new ArrayList<>();
        ArrayList<Garden> gardens = this.getGardens();

        for (Garden garden: gardens) {
            for (Plant plant: garden.getPlants()) {
                plants.add(plant);
            }
        }

        return plants;
    }

    public ArrayList<Plant> getMaturePlants() {

        ArrayList<Plant> maturePlants = new ArrayList<>();

        for(Plant plant: this.getPlants()) {
            if(plant.isMature(0))
                maturePlants.add(plant);
        }

        return maturePlants;
    }

    public void addGarden(Garden garden) {
        this.gardens.add(garden);
    }

    public void addPlant(Garden garden,Plant plant) {
        this.gardens.get(gardens.indexOf(garden)).addPlant(plant);
    }


    public int getIdPerson() {
        return idPerson;
    }

    public void setIdPerson(int idPerson) {
        this.idPerson = idPerson;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void save(){}

    public String toString()
    {
        return this.idPerson + "," + this.name + "," + this.firstname + "," + this.email;
    }
}
