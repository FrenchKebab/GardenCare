package com.gardencare.gardencare.Model;

import java.util.ArrayList;

public class Observable {

    private ArrayList<Observer> observers;

    public void attachObserver(Observer observer) {
        this.observers.add(observer);
    }

    public void removeObserver(Observer observer) {
        this.observers.remove(observer);
    }

    public void notify(Object objet) {

        for(Observer observer: this.observers) {
            observer.notify();
        }
    }
}
