package com.gardencare.gardencare.Model;
import com.gardencare.gardencare.Builder.PlantBuilder;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.NameValuePair;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.entity.UrlEncodedFormEntity;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;
import cz.msebera.android.httpclient.message.BasicNameValuePair;

public class Plant {
    private int idPlant;
    private Specie specie;
    private String mature;
    private Date plantedAt;


    public void setPlantedAt(Date plantedAt) {
        this.plantedAt = plantedAt;
    }

    public Date getPlantedAt() {
    	return this.plantedAt;
    }

    public Specie getSpecie() {
        return specie;
    }

    public String getWetHour() {
        return this.specie.getWetHour();
    }

    public String getName() {
        return this.specie.getName();
    }

    public int getGrowTime() {
        return this.specie.getGrowTime();
    }

    public String getWetAdvice() {
        return this.specie.getWetAdvice();
    }

    public void setSpecie(Specie specie) {
        this.specie = specie;
    }

    public int getIdPlant() {

        return idPlant;
    }

    public void setIdPlant(int idPlant) {
        this.idPlant = idPlant;
    }

    public String getImage() {
        return specie.getImage();
    }

    public String getMature() {
        return mature;
    }

    public void setMature(String mature) {
        this.mature = mature;
    }

    /**
     *
     * @param expand number of added days to growth time
     * @return mature or not
     */
    public boolean isMature(int expand) {
    	
        this.specie.setGrowTime(specie.getGrowTime() + expand);
    	
    	//Now
    	Date todayDate = new Date();
    	Calendar calNow = Calendar.getInstance();
    	calNow.setTime(todayDate);
    	
    	Calendar calPlant = this.getMaturityDate();

        return calPlant.equals(calNow) || calPlant.before(calNow);
    }

    public Calendar getMaturityDate() {

        Calendar calPlant = Calendar.getInstance();

        //Planted at this date
        calPlant.setTime(this.getPlantedAt());

        //Mature at this date
        calPlant.add(Calendar.DATE, this.specie.getGrowTime());

        return calPlant;
    }
    

    public void save(int idJardin) throws Exception {
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost("http://149.202.175.71/GardenCareService/public/plant/add");
        httpPost.addHeader("Content-type","application/x-www-form-urlencoded");

        Format formatter = new SimpleDateFormat("yyyy-MM-dd");

        BasicNameValuePair VidGarden = new BasicNameValuePair("garden",""+idJardin);
        BasicNameValuePair VidSpecie = new BasicNameValuePair("specie",""+this.specie.getIdSpecie());
        BasicNameValuePair VplantedAt = new BasicNameValuePair("plantedAt",formatter.format(this.plantedAt));

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(VidGarden);
        params.add(VidSpecie);
        params.add(VplantedAt);
        UrlEncodedFormEntity urlEncodedFormEntity = new UrlEncodedFormEntity(params,"UTF-8");
        httpPost.setEntity(urlEncodedFormEntity);

        HttpResponse httpResponse = httpClient.execute(httpPost);
        InputStream inputStream = httpResponse.getEntity().getContent();
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
        StringBuilder stringBuilder = new StringBuilder();
        String bufferedStrChunk = null;

        while ((bufferedStrChunk = bufferedReader.readLine()) != null) {
            stringBuilder.append(bufferedStrChunk);
        }
        JSONObject o = new JSONObject(stringBuilder.toString());
        Plant newPlant = (Plant) new PlantBuilder().build(Integer.parseInt(o.get("idPlant").toString()));

        this.setIdPlant(newPlant.getIdPlant());

    }

    public String toString()
    {
        Calendar cal = this.getMaturityDate();
        int day =  cal.get(Calendar.DAY_OF_MONTH);
        int month = cal.get(Calendar.MONTH);
        int year = cal.get(Calendar.YEAR);

        Calendar calPlantedAt = Calendar.getInstance();
        calPlantedAt.setTime(this.plantedAt);
        int dayPlantedAt =  calPlantedAt.get(Calendar.DAY_OF_MONTH);
        int monthPlantedAt = calPlantedAt.get(Calendar.MONTH);
        int yearPlantedAt = calPlantedAt.get(Calendar.YEAR);

        String str = "";
        str += this.specie.getName() +"\n";
        str += "Mature : " + year + "-0" +  (month+1) + "-0" + day + "\n";
        str += "PlantedAt : "+ yearPlantedAt + "-0" +  (monthPlantedAt+1) + "-0" + dayPlantedAt;

        return str;
    }
}
