package com.gardencare.gardencare.Builder;
import com.gardencare.gardencare.Model.Garden;
import com.gardencare.gardencare.Model.Person;
import com.google.gson.Gson;
import com.loopj.android.http.HttpGet;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URI;
import java.util.ArrayList;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

public class PersonBuilder extends Builder{
    private int id;
    private String email;

    public void setId(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return this.id;
    }

    public Object build(int id) throws Exception {
        this.setId(id);
        HttpClient client = new DefaultHttpClient();
        String query = "http://149.202.175.71/GardenCareService/public/person/" + this.getId();
        URI uri = new URI(query);
        HttpGet request = new HttpGet();
        request.setURI(uri);
        HttpResponse response = client.execute(request);
        String result = getResponse(response);
        JSONObject o = new JSONObject(result);

        Person person = new Person();
        person.setIdPerson(Integer.parseInt(o.get("idPerson").toString()));
        person.setFirstname(o.get("firstname").toString());
        person.setName(o.get("lastname").toString());
        person.setEmail(o.get("email").toString());
        person.setPassword(o.get("password").toString());
        person.setGardens(this.getGardens());

        return person;
    }

    @Override
    public Object build(String email) {
        try{
            this.setEmail(email);
            HttpClient client = new DefaultHttpClient();
            String query = "http://149.202.175.71/GardenCareService/public/person?email=" + email ;
            URI uri = new URI(query);
            HttpGet request = new HttpGet();
            request.setURI(uri);
            HttpResponse response = client.execute(request);
            String result = getResponse(response);
            JSONObject o = new JSONObject(result);
            Person person = new Person();
            person.setIdPerson(Integer.parseInt(o.get("idPerson").toString()));
            person.setFirstname(o.get("firstname").toString());
            person.setName(o.get("lastname").toString());
            person.setEmail(o.get("email").toString());
            person.setPassword(o.get("password").toString());
            person.setGardens(this.getGardens());

            return person;
        }
        catch(Exception e){
            return null;
        }

    }

    public ArrayList<Garden> getGardens() throws Exception {
        ArrayList<Garden> gardens = new ArrayList<>();
        HttpClient client = new DefaultHttpClient();
        String query = "http://149.202.175.71/GardenCareService/public/person/"+ this.getEmail() +"/gardens";
        URI uri = new URI(query);
        HttpGet request = new HttpGet();
        request.setURI(uri);
        HttpResponse response = client.execute(request);
        String result = getResponse(response);
        Gson gson = new Gson();
        JSONArray jsonArray = new JSONArray(result);
        ArrayList<String> list = new ArrayList<String>();
        for(int i = 0; i < jsonArray.length(); i++)
        {
            list.add(jsonArray.get(i).toString());
        }
        for(int j = 0 ; j < list.size() ; j++)
        {
            JSONObject o = new JSONObject(list.get(j));
            gardens.add((Garden) new GardenBuilder().build(Integer.parseInt(o.get("idGarden").toString())));
        }
        return gardens;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
