package com.gardencare.gardencare.Builder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import cz.msebera.android.httpclient.HttpResponse;

public abstract class Builder
{
    public abstract Object build(int id) throws Exception;

    public abstract Object build(String email) throws Exception;

    String getResponse(HttpResponse response) throws IOException {
        String result;
            InputStream in = response.getEntity().getContent();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder str = new StringBuilder();
            String line;
            while((line = reader.readLine()) != null)
            {
                str.append(line).append("\n");
            }
            in.close();
            result = str.toString();

        return result;
    }
}
